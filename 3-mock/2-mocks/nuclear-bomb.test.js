import { sendBombSignal, dieTogether } from './nuclear-bomb';

test('请测试 - sendBombSignal 会向 bomb 函数传递 O_o 作为起爆指令', () => {
  const myMock = jest.fn();
  sendBombSignal(myMock);
  expect(myMock.mock.calls[0][0]).toBe('O_o');
});
